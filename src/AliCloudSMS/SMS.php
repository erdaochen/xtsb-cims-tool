<?php
/**
 * Notes:短信发送/验证逻辑处理
 * author: chen
 * DateTime: 2022/7/7 14:25
 * @package Xtsb\Cims\AliCloudSMS
 */

namespace Xtsb\Cims\AliCloudSMS;

use Xtsb\Cims\Exception\ApiErrorDesc;
use Xtsb\Cims\Exception\ApiException;
use think\facade\Cache;

class SMS
{

  /**
   * Notes:发送验证码
   * author: chen
   * DateTime: 2022/7/7 14:51
   * @param $phone
   * @return bool
   */
  public static function sendCode($phone)
  {
//    $cacheCodeName = UID . $phone;//验证码缓存名称
//    $cacheCountName = UID . $phone . 'count';//短信发送次数缓存名称
//    $cacheTimeName = UID . $phone . 'time';//一分钟只可短信一条短信缓存名称

    $cacheCodeName = $phone;//验证码缓存名称
    $cacheCountName = $phone . '_count';//短信发送次数缓存名称
    $cacheTimeName = $phone . '_time';//一分钟只可短信一条短信缓存名称

    $count = Cache::store('redis')->get($cacheCountName, 0);
    if ($count > 10) {
      throw new ApiException(ApiErrorDesc::SMS_COUNT_ERROR);
    }
    // 校验手机号
    if (!preg_match('/^1[3456789]\d{9}$/', $phone)) {
      throw new ApiException(ApiErrorDesc::ERROR_FORMAT, '手机号码');
    }

    $cacheTime = Cache::store('redis')->get($cacheTimeName, 0);
    if ($cacheTime == 1) {
      throw new ApiException(ApiErrorDesc::SMS_REPEAT_ERROR);
    }

    $code = rand(100000, 999999);
    //发送短信
    $result = AliCloud::sendSms($phone, 'SMS_196656268', ['code' => $code]);

    if ($result) {
      // 设置缓存
//      Cache::store('redis')->set($cacheCodeName, $code, time() + 10 * 60);//10分钟
//      // 设置一分钟后可重新获取短信
//      Cache::store('redis')->set($cacheTimeName, 1, time() + 60);//一分钟
//      Cache::store('redis')->set($cacheCountName, $count + 1, time() + 3600 * 24); //24小时

      // 设置缓存
      Cache::store('redis')->set($cacheCodeName, $code, 10 * 60);//10分钟
      // 设置一分钟后可重新获取短信
      Cache::store('redis')->set($cacheTimeName, 1, 60);//一分钟
      Cache::store('redis')->set($cacheCountName, $count + 1, 3600 * 24); //24小时

      return true;
    }

    return false;
  }


  /**
   * Notes: 验证码校验
   * author: chen
   * DateTime: 2022/7/7 14:50
   * @param $phone
   * @param $code
   * @return bool
   */
  public static function checkCode($phone, $code)
  {
//    $cacheCodeName = UID . $phone;//验证码缓存名称
//    $cacheCountName = UID . $phone . 'count';//短信发送次数缓存名称
//    $cacheTimeName = UID . $phone . 'time';//一分钟只可短信一条短信缓存名称

    $cacheCodeName = $phone;//验证码缓存名称
    $cacheCountName = $phone . '_count';//短信发送次数缓存名称
    $cacheTimeName = $phone . '_time';//一分钟只可短信一条短信缓存名称

    $count = Cache::store('redis')->get($cacheCountName, 0);
    if ($count > 10) {
      throw new ApiException(ApiErrorDesc::SMS_COUNT_ERROR);
    }
    // 校验手机号
    if (!preg_match('/^1[3456789]\d{9}$/', $phone)) {
      throw new ApiException(ApiErrorDesc::ERROR_FORMAT, '手机号码');
    }

    if (!Cache::store('redis')->get($cacheCodeName)) {
      throw new ApiException(ApiErrorDesc::SMS_CODE_EXPIRE);
    }

    //验证码
    $smsCode = Cache::store('redis')->get($cacheCodeName);
    if ($smsCode != $code) {
      throw new ApiException(ApiErrorDesc::SMS_CODE_ERROR);
    }

    // 验证通过设置缓存失效
    Cache::store('redis')->delete($cacheCodeName);
    Cache::store('redis')->delete($cacheTimeName);

    Cache::store('redis')->set($cacheCountName, $count + 1, 600);//10分钟

    return true;
  }

  public static function regularNotice($phone,$time,$num){
    $res = AliCloud::sendSms($phone, 'SMS_464095048', ['time' => $time, 'count'=>$num]);
    //fail([$res,$phone,$time,$num]);
  }

}
