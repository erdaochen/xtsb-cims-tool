<?php

namespace Xtsb\Cims\Btn;

use think\facade\Db;

class TableDataChangeHelper
{

  /**根据用户的输入 获取表字段的注释
   * @param $list array ['title','update_time']
   * @param $table_name string cca
   */
  static function fillCommentByMatchWithKey(&$list,$table_name){
    $list = array_flip($list);  // ['title'=>'','update_time'=>''];

    $columns = Db::query(" SELECT
                            COLUMN_NAME NAME,
                            COLUMN_COMMENT COMMENT
                          FROM
                            INFORMATION_SCHEMA. COLUMNS
                          WHERE
                            table_name = '$table_name'
                          AND table_schema = 'xtsb_cims'");

    foreach ($columns as $column){
      $name = $column['NAME'];
      $comment = $column['COMMENT'];
      if(isset($list[$name])){
        $list[$name] = $comment;
      }
    }
    // ['title'=>'标题','update_time'=>'更新时间']
  }

  /**原有的表数据与用户输入数据比对
   * @param $old array
   * @param $new array
   * @param $descList array
   * @return string
   */
  static function changeContent($old,$new,$descList){
    $text = "";
    $new_handler = $new['handler']??"";
    $old_handler = $old['handler']??"";
    if($new_handler != $old_handler){
      $names = Db::name("staff")->whereIn('uid',[$new_handler,$old_handler])->column("name",'uid');
      $old['handler'] = $names[$old_handler]??"";
      $new['handler'] = $names[$new_handler]??"";
    }
    foreach ($descList as $name=>$desc){
      if(isset($new[$name]) && ($old[$name] != $new[$name])){
        $text.="{$desc}:{$old[$name]}变更为{$new[$name]}，";
      }
    }
    return $text;
  }
}