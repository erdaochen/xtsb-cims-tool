<?php
/**
 * ButtonData.php
 * Notes:
 * author: chen
 * DateTime: 2022/4/13 16:16
 * @package app\http\decorators
 */

namespace Xtsb\Cims\Btn;

class ButtonData
{
  /**
   * Notes:过滤无权限按键
   * author: chen
   * DateTime: 2022/2/8 18:01
   * @param array $btns
   * @return array
   */
  public static function render($btns = [])
  {

    $request = app('request');
    $isApp = self::isApp($request);
    if (isset($request->userInfo->uid) && $request->userInfo->uid === 1) {
      //超级管理拥有所有权限
      $buttons = [];
      foreach ($btns as $btn) {
        if (self::isBan($btn, $isApp)) {
          continue;
        }
        if (isset($btn['btnList']) && count($btn['btnList']) > 0) {
          $btnList = self::render($btn['btnList']);
          if (count($btnList) > 0) {
            $btn['btnList'] = $btnList;
            $buttons[] = $btn;
          }
        } else {
          $buttons[] = $btn;

        }
      }
      return $buttons;
    }

    $urls = $request->rules;
//    $urls[] = request()->controller . '/import';
//    $urls[] = request()->controller . '/export';
//    dd($urls);
    $buttons = [];
    if (!empty($btns)) {
      foreach ($btns as $key => $btn) {
        $dataUrl = isset($btn['data_url']) ? $btn['data_url'] : '/';
        if (
//          strpos($dataUrl, 'xtsb/') !== false||
          in_array(self::routeToUrl($dataUrl, $btn['type']), $urls)
          || (isset($btn['noAuth']) && $btn['noAuth'])) {
          //统计表的跳转，通过列表的url跳转有问题，暂时先用btn来处理，需要兼容xtsb/chart/xxx/index的写法   lizhongjie 20221221
          if (strpos($dataUrl, 'xtsb/') === false || strpos($dataUrl, '/index') !== false) {
            //过滤 /index字符串
            $dataUrl = str_replace('/index', '', $dataUrl);
            $dataUrl = str_replace('xtsb/', '', $dataUrl);
          }

//          if (isset($btn['querys']) && !empty($btn['querys']) && is_array($btn['querys'])) {
//            if (strpos($dataUrl, '?') === false) {
//              $dataUrl .= "?";
//            }
//            foreach ($btn['querys'] as $query) {
//              $dataUrl .= "&" . $query["name"] . "=" . $query["value"];
//            }
//          }
          if(self::isBan($btn,$isApp)){
            continue;
          }
          unset($dataUrl);

          if (isset($btn['btnList']) && count($btn['btnList']) > 0) {
            $btnList = self::render($btn['btnList']);
            if (count($btnList) > 0) {
              $btn['btnList'] = $btnList;
              $buttons[] = $btn;
            }
          } else {
            $buttons[] = $btn;
          }
          unset($btn);
        }
        else if($btn['type']=='href'){
          $buttons[] = $btn;
        }
      }
    }
    unset($btns);
//    die;
    return $buttons;
  }


  /**
   * 是否禁止
   * @param $btn
   * @param false $isApp
   * @return bool
   */
  private static function isBan($btn, $isApp = false)
  {
    // 禁止地址
    $banUrls = [
      //工程队
      'team_settlement_pay',//批量付款
      'team_salary',//工程队-工资制单
//      'team_borrow',//工程队-借款申请

      // 员工
      'staff_salary',//工资发放

      //采购
//      'goods_purchase_pay',//采购付款
//      'minor_purchase',//零星采购

      // 下游付款
//      'finance_settlement',//下游付款
    ];
    // 禁止类型
    $banTypes = ['edit', 'create'];// 编辑

    $isBan = false;

    // 是否手机端
    if ($isApp) {
      $type = $btn['type'] ?? null;
      $url = $btn['url'] ?? null;
      if (in_array($type, $banTypes) && in_array($url, $banUrls)) {
        $isBan = true;
      }
    }
    return $isBan;
  }

  /**
   * 是否app访问
   * @param $request
   * @return bool
   */
  private static function isApp($request)
  {
    $isApp = false;
    if ($request->httpUserAgent != 'window') {
      $isApp = true;
    }
    return $isApp;
  }


  /**
   * Notes:横杠转驼峰
   * author: chen
   * @param $route string 路由
   * @param $btnType null 按键类型
   * @param $separator
   * DateTime: 2022/2/8 17:33
   * @return string
   */
  private static function routeToUrl($route, $btnType = null, $separator = '_')
  {

    $routes = explode('/', $route);

    if (strtolower($routes[0]) == 'xtsb') {
      $controllerName = str_replace(" ", "", ucwords(str_replace($separator, " ", $routes[1])));//首字母大写
      $actionName = $routes[2];
    } else {
      $controllerName = str_replace(" ", "", ucwords(str_replace($separator, " ", $routes[0])));//首字母大写
      $actionName = $routes[1] ?? 'index';
    }

    if (in_array($btnType, ['create', 'edit', 'status', 'delete', 'upload'])) {
      $actionName = 'create';

    } elseif (in_array($btnType, ['index', 'read', 'download', 'preview', 'dropdown','print'])) {
      $actionName = 'index';

    }

    return $controllerName . '/' . $actionName;
  }
}
