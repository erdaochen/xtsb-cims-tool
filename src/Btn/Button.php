<?php

namespace Xtsb\Cims\Btn;

class Button
{
  protected static $instance = null;

//  private static $btn = [
//    'name' => null,
//    'type' => null,
//    'component' => null,
//    'requestApi' => null,
//    'postApi' => null,
//    'save' => false,
//    'submit' => false,
//    'querys' => null,
//    'list' => null,
//  ];
  private static $btn = [];
  public static $arr = [];
  public static $currentName = null;

  public function __construct()
  {

  }

  public function __clone()
  {

  }

  //渲染出按键
  public function render()
  {
    $this->create();
//    dd(self::$btn);
//    $buttons[] = array_values(self::$btn);
    $buttons[] = self::$btn;

    self::$btn = [];
    return ButtonFilter::render($buttons);
  }

  private static function currentName()
  {
    return self::$currentName ?? self::$arr[0];
  }

  //初始化静态属性
  private function initStaticProperties()
  {
//    $class = new \ReflectionClass('\Xtsb\Cims\Btn\Button');
//    $properties = array_keys($class->getStaticProperties());
//    foreach ($properties as $property) {
//      if ($property === 'instance') {
//        continue;
//      }
//      self::$instance::$property = null;
//    }

//    $properties = array_keys(self::$btn);
//    foreach ($properties as $property) {
//      self::$btn[self::currentName()][$property] = null;
//    }

    self::$btn['querys'] = [];
//    unset(self::$btn[self::currentName()]);
  }

  //创建按键名称
  public static function name($value)
  {
    if (!self::$instance) {
      self::$instance = new self();
    }


//    self::$arr[] = $value;
//    self::$currentName = $value;//当前按键名称
//    self::$btn[self::currentName()]['title'] = $value;
    self::$btn = [];
    self::$btn['title'] = $value;

    return self::$instance;
  }

  //按键类型
  public function type($value)
  {

//    self::$btn[self::currentName()]['type'] = $value;
    self::$btn['type'] = $value;
    return $this;
  }

  //是否开放权限
  public function noAuth()
  {
//    self::$btn[self::currentName()]['noAuth'] = true;
    self::$btn['noAuth'] = true;
    return $this;
  }

  //按键绑定的组件名称
  public function component($value)
  {

//    self::$btn[self::currentName()]['component'] = $value;
    self::$btn['component'] = $value;
    return $this;
  }

  //数据获取接口
  public function request($value)
  {

//    self::$btn[self::currentName()]['date_url'] = $value;
    self::$btn['data_url'] = $value;
    return $this;
  }

  //数据获取接口
  public function post($value)
  {

//    self::$btn[self::currentName()]['url'] = $value;
    self::$btn['url'] = $value;
    return $this;
  }

  //按键绑定的组件弹窗是否显示保存按键
  public function save($value = true)
  {

//    self::$btn[self::currentName()]['save'] = $value;
    self::$btn['save'] = $value;
    return $this;
  }

  //按键绑定的组件弹窗是否显示提交按键
  public function submit($value = true)
  {

//    self::$btn[self::currentName()]['submit'] = $value;
    self::$btn['submit'] = $value;
    return $this;
  }

  public function querys(...$value)
  {

//    self::$btn[self::currentName()]['querys'] = $value;
    self::$btn['querys'] = $value;
    return $this;
  }

  //按键绑定的条件
  public function query($name, $value)
  {

//    self::$btn[self::currentName()]['querys'][] = ['name' => $name, 'value' => $value];
    self::$btn['querys'][] = ['name' => $name, 'value' => $value];
    return $this;
  }

  //子按键
  public function list($btn)
  {
//dd(self::$arr);
//    self::$btn[self::currentName()]['btnList'] = $btn;
    self::$btn['btnList'] = $btn;
    return $this;
  }

  //生成按键
  public function create()
  {
//    $res = [
//      'type' => self::$type,
//      'title' => self::$name,
//      'component' => self::$component,
//      'data_url' => self::$requestApi,
//      'url' => self::$postApi,
//      'save' => self::$save,
//      'submit' => self::$submit,
//      'querys' => self::$querys,
//      'btnList' => self::$list,
//    ];
//    $res = self::$btn[self::currentName()];
    self::$btn['save'] = isset(self::$btn['save']) ?? false;
    self::$btn['submit'] = isset(self::$btn['submit']) ?? false;

    $res = self::$btn;

    self::initStaticProperties();

    return $res;
  }
}