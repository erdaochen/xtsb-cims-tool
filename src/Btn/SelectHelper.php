<?php

namespace Xtsb\Cims\Btn;
use think\facade\Db;
class SelectHelper
{

  static function branchList($where=['cuid' => CUID, 'status' => 1]){
    return Db::name('branch')
      ->field('id value,title label')
      ->where($where)
      ->select()
      ->toArray();
  }

  static function dmList($where=[]){
    return Db::table(DM_NAME)
      ->alias("g")
      ->field("g.id value,g.dm_name label,g.branch_id")
      ->where($where)
      ->whereNotNull("branch_id")
      ->select();
  }

  static function projectAddress(){
    return Db::name('dm')
      ->field('project_addr')
      ->json(['project_addr'])
      ->where('id', DMID)
      ->find();
  }

  static function listByArray($data){
    $list =[];
    foreach ($data as $each){
      $list[] = ['value'=>$each,'label'=>$each];
    }
    return $list;
  }

  static function listByKvArray($data){
    $list =[];
    foreach ($data as $value=>$label){
      $list[] = ['value'=>$value,'label'=>$label];
    }
    return $list;
  }

}