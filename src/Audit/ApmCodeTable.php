<?php
/**
 * ApmCodeTable.php
 * Notes:
 * author: chen
 * DateTime: 2022/5/20 18:35
 * @package Xtsb\Cims\Audit
 */

namespace Xtsb\Cims\Audit;

class ApmCodeTable
{

  const LIST = [
    'branch_apply_break' => 'branch_apply',
    'project_plan_make' => 'project_plan',
    'goods_purchase_donor_return' => 'goods_purchase',
    'goods_purchase_donor' => 'goods_purchase',
    'inventory_in' => 'inventory_change',
    'inventory_out' => 'inventory_change',
    'team_salary' => 'staff_salary',
    'team_settlement_pay' => 'team_settlement_pay',
    'my_imprest' => 'cost_apply',
    'imprest' => 'cost_apply',

    'my_cost' => 'cost_apply',
    'ca' => 'cost_apply',
    'rode' => 'cost_apply',
    'rome' => 'cost_apply',
    'ca_expense' => 'cost_apply',
    'rode_expense' => 'cost_apply',
    'rome_expense' => 'cost_apply',
    'project_advance_pay' => 'cost_apply',

    'team_ledger_adjust' => 'team_goods_change',
    'team_ledger_pay' => 'team_goods_change',
    'team_ledger_return' => 'team_goods_change',
    'team_goods_return' => 'team_goods_change',
    'team_goods_pick' => 'team_goods_change',
    'goods_return_cor' => 'goods_purchase',
    'goods_pick_cor' => 'goods_purchase',
    'manufacture_goods_purchase' => 'goods_purchase',
    'asset_dispose' => 'asset_record',
    'asset_transfer' => 'asset_record',
    'asset_use' => 'asset_record',
    'asset_upkeep' => 'asset_record',
    'asset_repair' => 'asset_record',

    'team_borrow' => 'cost_apply',
    'other_company_goods_borrow' => 'goods_purchase',
    'other_company_goods_return' => 'goods_purchase',
	  'company_goods_borrow' => 'goods_purchase',
	  'company_goods_return' => 'goods_purchase',

    'project_receive' => 'project_task_record',
    'project_foundation' => 'project_task_record',
    'project_start' => 'project_task_record',
    'project_complete' => 'project_task_record',
    'project_dwg' => 'project_task_record',
    'project_cad' => 'project_task_record',
    'project_cad_confirm' => 'project_task_record',
    'project_accept_material' => 'project_task_record',
    'project_accept_submit' => 'project_task_record',
    'project_accept' => 'project_task_record',
    'project_maintain' => 'project_task_record',
    'project_team_assign'=> 'project_task_record',

    'project_audit_material' => 'project_task_record',
    'project_audit_submit' => 'project_task_record',
    'project_audit_settlement' => 'project_task_record',
    'project_audit_result' => 'project_task_record',
    'project_audit_document' => 'project_task_record',
    'project_audit_archives' => 'project_task_record',
    'project_audit_pay' => 'project_task_record',
  
    'project_cancel' => 'project_task_record',
    'project_recover' => 'project_task_record',

    'finance_schedule_income' => 'finance_schedule',
    'work_safety_check' => 'safety_inspect',
    'policy' => 'insure_policy_order',
    'endorse' => 'insure_endorse_order',

    'exchange_income' => 'finance_income',
    'repayment_income' => 'finance_income',
    'first_party' => 'goods_supplier',
    'finance_settlement_invoice'=> 'finance_settlement',

    'downstream_pay'  => 'imprest_detail',
    'minor_purchase' => 'goods_purchase',
    'attendance_report_by_week'=>'attendance_report',
    'mes_sale_order'=>'mes_eorder_manage',

    'seal'=>'seal',
    'file_apply_credit'=>'file_apply'
  ];


  public static function list()
  {
    return self::LIST;
  }

  public static function getTableName($tableName)
  {
    return self::LIST[$tableName] ?? null;
  }
}
