<?php
/**
 * Status.php
 * Notes:获取模块对应的审批状态
 * author: chen
 * DateTime: 2022/5/19 19:54
 * @package Xtsb\Cims\Audit
 */

namespace Xtsb\Cims\Audit;

class Status
{

  /**
   * @id number 状态id
   * @title string 状态名称
   * @code array 含有该状态的模块名称
   * @is_show bool 状态在流程配置中是否显示（不显示 则仅系统可调用）
   * @sort number 排序
   * @action string 前端操作组件名称 null：无操作动作/是否完成（流程设置后 则认为该步骤结束 业务完成）
   * @alias string 操作动作名称 action=null：alias=null
   */
  const LIST = [
    ['id' => 1, 'title' => '草稿', 'code' => [], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => '提交'],
    ['id' => 2, 'title' => '审批完成', 'code' => [], 'is_show' => 1, 'sort' => 1, 'action' => null, 'alias' => '审批完成'],
    ['id' => 3, 'title' => '审批中', 'code' => [], 'is_show' => 1, 'sort' => 1, 'action' => 'audit', 'alias' => '审批'],
    ['id' => 4, 'title' => '审批驳回', 'code' => [], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => '审批'],

    ['id' => 5, 'title' => '待盖章',
      'code' => ['cca', 'sub_cca', 'cor', 'sub_cor', 'maintain_cca', 'maintain_cor', 'goods_purchase', 'team_protocol', 'seal', 'branch_apply', 'branch_apply_break'], 'is_show' => 1, 'sort' => 1,
      'action' => 'stamp', 'alias' => '盖章'],

    ['id' => 6, 'title' => '撤回', 'code' => [], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => '提交'],

    ['id' => 7, 'title' => '待签收',
      'code' => ['finance_invoice', 'finance_invoice_invalid', 'finance_cost_invoice_invalid', 'cca', 'sub_cca', 'cor', 'sub_cor', 'maintain_cca', 'maintain_cor', 'goods_purchase', 'team_protocol', 'finance_tax_certificate', 'branch_apply', 'branch_apply_break'], 'is_show' => 1, 'sort' => 1,
      'action' => 'rece', 'alias' => '签收'],
    ['id' => 8, 'title' => '待寄送',
      'code' => ['cca', 'sub_cca', 'cor', 'sub_cor', 'maintain_cca', 'maintain_cor', 'finance_invoice', 'finance_invoice_invalid', 'finance_cost_invoice_invalid', 'goods_purchase', 'team_protocol', 'finance_tax_certificate', 'seal', 'branch_apply', 'branch_apply_break'], 'is_show' => 1, 'sort' => 1,
      'action' => 'send_out', 'alias' => '寄送'],
    ['id' => 9, 'title' => '确认编号',
      'code' => ['cca', 'cor', 'project_ledger', 'sub_cca', 'sub_cor'], 'is_show' => 1, 'sort' => 1,
      'action' => 'order_no_confirm', 'alias' => '确认编号'],


    ['id' => 10, 'title' => '到款确认', 'code' => ['finance_income', 'team_ledger_pay'], 'is_show' => 1, 'sort' => 1,
      'action' => 'finance_income_confirm', 'alias' => '到款确认'],
    ['id' => 11, 'title' => '使用中',
      'code' => ['cca', 'sub_cca', 'cor', 'sub_cor', 'maintain_cca', 'maintain_cor', 'oil_engine_apply', 'goods_purchase', 'team_protocol'], 'is_show' => 1, 'sort' => 1,
      'action' => 'return_back', 'alias' => '归还'],
    ['id' => 12, 'title' => '待开票', 'code' => ['finance_invoice'], 'is_show' => 1, 'sort' => 1,
      'action' => 'invoice', 'alias' => '开票'],
    ['id' => 13, 'title' => '发票作废', 'code' => ['finance_invoice_invalid', 'finance_cost_invoice_invalid'], 'is_show' => 1, 'sort' => 1,
      'action' => 'invoice_invalid', 'alias' => '发票作废'],
    ['id' => 14, 'title' => MANAGE_INCOME_NAME . '核算', 'code' => ['finance_settlement_invoice'], 'is_show' => 1, 'sort' => 1,
      'action' => 'manage_calc', 'alias' => MANAGE_INCOME_NAME . '核算'],
    ['id' => 15, 'title' => '成本核算', 'code' => ['finance_settlement_invoice'], 'is_show' => 1, 'sort' => 1,
      'action' => 'cost_calc', 'alias' => '成本核算'],
    ['id' => 16, 'title' => '中标结果跟踪', 'code' => ['bid'], 'is_show' => 1, 'sort' => 1,
      'action' => 'outbid', 'alias' => '中标结果跟踪'],
    ['id' => 17, 'title' => '待归档',
      'code' => ['cca', 'sub_cca', 'cor', 'sub_cor', 'maintain_cca', 'maintain_cor', 'goods_purchase', 'team_protocol', 'project_audit', 'seal'], 'is_show' => 1, 'sort' => 1,
      'action' => 'arch', 'alias' => '归档'],
    ['id' => 18, 'title' => '待开外管证', 'code' => ['finance_invoice', 'finance_tax_certificate'], 'is_show' => 1, 'sort' => 1,
      'action' => 'wgzkj', 'alias' => '外管证开具'],
    ['id' => 19, 'title' => '替换流程', 'code' => [], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => '提交'],
    ['id' => 20, 'title' => '变更步骤', 'code' => [], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => '审批'],
//    ['id' => 21, 'title' => '已开票', 'code' => [], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => null],
    ['id' => 22, 'title' => '审计结果录入', 'code' => ['project_audit'], 'is_show' => 1, 'sort' => 1,
      'action' => 'project_audit', 'alias' => '审计结果录入'],
    ['id' => 23, 'title' => '投标资料准备', 'code' => ['bid'], 'is_show' => 1, 'sort' => 1,
      'action' => 'bid_file_start', 'alias' => '投标资料准备'],
    ['id' => 24, 'title' => '外管证缴销', 'code' => ['finance_invoice', 'finance_tax_certificate'], 'is_show' => 1, 'sort' => 1,
      'action' => 'jx', 'alias' => '外管证缴销'],
    ['id' => 25, 'title' => '待付款', 'code' => ['goods_purchase_pay', 'team_settlement_pay', 'my_imprest', 'finance_settlement',
      'team_salary', 'staff_salary', 'ca', 'finance_settlement_pay', 'team_borrow', 'rome', 'rode', 'oil_card_record', 'minor_purchase','project_advance_pay'], 'is_show' => 1, 'sort' => 1,
      'action' => 'remit', 'alias' => '付款'],
    ['id' => 26, 'title' => '转交', 'code' => [], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => '转交'],
    ['id' => 27, 'title' => '标书制作', 'code' => ['bid_task'], 'is_show' => 1, 'sort' => 1,
      'action' => 'bid_file', 'alias' => '标书制作'],
    ['id' => 28, 'title' => '初审', 'code' => ['bid_task', 'design_task'], 'is_show' => 1, 'sort' => 1,
      'action' => 'bid_first_review', 'alias' => '初审'],
    ['id' => 29, 'title' => '终审', 'code' => ['bid_task', 'design_task'], 'is_show' => 1, 'sort' => 1,
      'action' => 'bid_last_review', 'alias' => '终审'],
    ['id' => 30, 'title' => '红字发票信息表附件上传', 'code' => ['finance_cost_invoice_invalid'], 'is_show' => 1, 'sort' => 1,
      'action' => 'red_info_notifier_file', 'alias' => '红字发票信息表附件上传'],


//    ['id' => 31, 'title' => '结算确认', 'code' => ['finance_settlement'], 'is_show' => 1, 'sort' => 1,
//      'action' => 'finance_settlement_confirm', 'alias' => '结算确认'],


    ['id' => 32, 'title' => '扫描上传', 'code' => ['cca', 'sub_cca', 'cor', 'sub_cor', 'maintain_cca', 'maintain_cor', 'goods_purchase', 'team_protocol'], 'is_show' => 1, 'sort' => 1,
      'action' => 'scan_contract', 'alias' => '扫描上传'],
    ['id' => 33, 'title' => '成本月份确认', 'code' => ['ca', 'rome', 'rode'], 'is_show' => 1, 'sort' => 1,
      'action' => 'com', 'alias' => '成本月份确认'],

    ['id' => 34, 'title' => '转正', 'code' => ['staff_status_up'], 'is_show' => 1, 'action' => 'staff_status_up_confirm', 'sort' => 1, 'alias' => '转正'],
    ['id' => 35, 'title' => '离职', 'code' => ['staff_status_off'], 'is_show' => 1, 'action' => 'staff_status_off_confirm', 'sort' => 1, 'alias' => '离职'],
    ['id' => 36, 'title' => '加班通过确认', 'code' => ['attendance_overwork'], 'is_show' => 1, 'action' => 'attendance_overwork_confirm', 'sort' => 1, 'alias' => '加班通过确认'],


    ['id' => 51, 'title' => '费用确定', 'code' => ['rode', 'ca', 'rome'], 'is_show' => 1, 'sort' => 1,
      'action' => 'cost_confirm', 'alias' => '费用确定', 'is_hide_detail' => true],

    ['id' => 60, 'title' => '网银支付中', 'code' => ['remit'], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => '网银支付'],

    ['id' => 62, 'title' => '初步设计', 'code' => ['design_task'], 'is_show' => 1, 'sort' => 1,
      'action' => 'design_task_first', 'alias' => '初步设计'],
    ['id' => 63, 'title' => '深化设计', 'code' => ['design_task'], 'is_show' => 1, 'sort' => 1,
      'action' => 'design_task_deep', 'alias' => '深化设计'],
    ['id' => 65, 'title' => '库存盘点', 'code' => ['inventory_check'], 'is_show' => 1, 'sort' => 1,
      'action' => 'inventory_check_confirm', 'alias' => '库存盘点'],

    ['id' => 66, 'title' => '抽检登记', 'code' => ['project_inspect_qc'], 'is_show' => 0, 'sort' => 1, 'action' => null, 'alias' => null],
    ['id' => 68, 'title' => '回单登记', 'code' => ['goods_purchase_donor', 'goods_purchase_donor_return'], 'is_show' => 1, 'sort' => 1,
      'action' => 'purchase_donor_file', 'alias' => '回单登记'],
    ['id' => 69, 'title' => '拜访结果录入', 'code' => ['sale_lead_track'], 'is_show' => 1, 'sort' => 1,
      'action' => 'sale_lead_file', 'alias' => '拜访结果录入'],
//    ['id' => 70, 'title' => '立项确认', 'code' => ['cor'], 'is_show' => 1,   'sort' => 1,
//      'action' => 'back', 'alias' => '退回'],

    ['id' => 71, 'title' => '其他类型到款确认', 'code' => ['exchange_income'], 'is_show' => 1, 'sort' => 1,
      'action' => 'exchange_income', 'alias' => '其他类型到款确认'],

    ['id' => 72, 'title' => '资料校验', 'code' => ['file_aver'], 'is_show' => 1, 'sort' => 1,
      'action' => 'batch_attach', 'alias' => '资料校验'],

    ['id' => 73, 'title' => '待资料归档', 'code' => ['file_aver'], 'is_show' => 1, 'sort' => 1,
      'action' => 'batch_arch', 'alias' => '资料归档'],


    ['id' => 100, 'title' => '已核销', 'code' => [''], 'is_show' => 0, 'action' => null, 'sort' => 1],
    ['id' => 101, 'title' => '已取消', 'code' => ['cca', 'cor', 'maintain_cca', 'maintain_cor', 'project_ledger', 'safety_assess'], 'is_show' => 0, 'action' => null, 'sort' => 1],
    ['id' => 102, 'title' => '已关闭', 'code' => [''], 'is_show' => 0, 'action' => null, 'sort' => 1],
    ['id' => 103, 'title' => '已归档', 'code' => [''], 'is_show' => 0, 'action' => null, 'sort' => 1],

  ];


  //获取状态列表
  public static function list($code = null, $checkShow = false)
  {
    $arr = [];
    $resultArr = self::LIST;
    if (isset($code)) {
      foreach ($resultArr as $key => $value) {

        if ($checkShow && $value['is_show'] == 0) { //过滤隐藏状态
          continue;
        }
        if (in_array($code, $value['code']) || empty($value['code'])) {
          $arr[] = ['value' => $value['id'], 'label' => $value['title'], 'action' => $value['action'] ?? null, 'code' => $value['code']];
        }
      }
    } else {
      foreach ($resultArr as $key => $value) {

        if ($checkShow && $value['is_show'] == 0) { //过滤隐藏状态
          continue;
        }
        $arr[] = ['value' => $value['id'], 'label' => $value['title'], 'action' => $value['action'] ?? null, 'code' => $value['code']];
      }
    }
    return $arr;
  }

  /**
   * Notes:
   * author: chen
   * DateTime: 2022/5/23 14:44
   * @param $id int 状态id
   * @param false $isStyle 是否带样式
   * @return mixed|string
   */
  public static function getStatusName($id, $isStyle = false, $name = 'title')
  {
    $arr = array_column(self::LIST, $name, 'id');

    if (!isset($arr[$id])) {
      return '--';
    }
    if ($isStyle) {
      if ($id == 1) {
        return '<span style="color: black">' . $arr[$id] . '</span>';
      }
      if ($id == 4) {
        return '<span style="color: red">' . $arr[$id] . '</span>';
      }
      if ($id == 6 || $id == 101) {
        return '<span style="color: darkgray">' . $arr[$id] . '</span>';
      }
      if (in_array($id, self::completeStatusIds())) {
        return '<span style="color: green">' . $arr[$id] . '</span>';
      }

      return '<span style="color: #409eff">' . $arr[$id] . '</span>';
    }

    return $arr[$id];
  }


  public static function statusNames()
  {
    return array_column(self::LIST, 'title', 'id');
  }

  /**
   * Notes:
   * author: chen
   * DateTime: 2023/2/21 10:08
   * @param string $name title | alias
   * @return array
   */
  public static function statusIds($name = 'title')
  {
    return array_column(self::LIST, 'id', $name);
  }


//  private static function isCompleteStatus($id)
//  {
//    $arr = array_column(self::LIST, 'action', 'id');
//
//    return isset($arr[$id]);
//  }

  //完成状态的status $filters需要过滤的status id
  public static function completeStatusIds($filters = [])
  {
    $resultArr = self::LIST;
    $arr = [];
    foreach ($resultArr as $key => $value) {
      if (!isset($value['action']) && !in_array($value['id'], $filters)) { //过滤完成状态
        $arr[] = $value['id'];
      }
    }
    unset($resultArr);
    return $arr;
  }

  ###########################################################################################

  /**
   * Notes:审批组件名称---审批名称 数组
   * author: chen
   * DateTime: 2022/6/28 17:03
   * @param null $code
   * @return array
   */
  public static function actionList($code = null)
  {
    $arr = [];
    $resultArr = self::LIST;
    if (isset($code)) {
      foreach ($resultArr as $key => $value) {
        if (isset($value['action'])) { //过滤完成状态
          if (in_array($code, $value['code']) || empty($value['code'])) {
            $arr[] = ['value' => $value['action'], 'label' => $value['alias'], 'code' => $value['code']];
          }
        }
      }
    } else {
      foreach ($resultArr as $key => $value) {
        if (isset($value['action'])) { //过滤完成状态
          $arr[] = ['value' => $value['action'], 'label' => $value['alias'], 'code' => $value['code']];
        }
      }
    }
    return $arr;
  }

  /**
   * Notes: status id---审批名称 数组
   * author: chen
   * DateTime: 2022/6/28 17:03
   * @param null $code
   * @param array $status 当前状态值
   * @return array
   */
  public static function actionIdList($code = null, $status = null)
  {
    //判断是否仅返回完成状态
    if (isset($status) && in_array($status, [25])) {
      return [
        ['value' => 2, 'label' => '完成', 'code' => null]
      ];
    }

    $arr = [];
    $resultArr = self::LIST;
    if (isset($code)) {
      foreach ($resultArr as $key => $value) {
        if (isset($value['action']) || $value['id'] == 2) { //过滤完成状态
          if (in_array($code, $value['code']) || empty($value['code'])) {
            $arr[] = ['value' => $value['id'], 'label' => $value['alias'], 'code' => $value['code']];
          }
        }
      }
    } else {
      foreach ($resultArr as $key => $value) {
        if (isset($value['action']) || $value['id'] == 2) { //过滤完成状态
          $arr[] = ['value' => $value['id'], 'label' => $value['alias'], 'code' => $value['code']];
        }
      }
    }
    return $arr;
  }

  //根据status 返回 审批组件名称
  public static function getActionName($statusId)
  {
    $arr = array_column(self::LIST, 'action', 'id');
    return $arr[$statusId];
  }

  /**
   * Notes:审批组件名称->状态别名
   * author: chen
   * DateTime: 2022/6/28 16:59
   * @return array
   */
  public static function actionNames()
  {
    return array_column(self::LIST, 'alias', 'action');
  }

  /**
   * Notes: 状态别名->审批操作code
   * author: chen
   * DateTime: 2022/6/28 16:58
   * @return array
   */
  public static function actionIds()
  {
    return array_column(array_filter(self::LIST, function ($item) {
      if ($item['action']) {
        return $item;
      }
    }), 'action', 'alias');
  }

  /**
   * Notes: action->隐藏详情页
   * author: chen
   * DateTime: 2022/6/28 16:58
   * @return array
   */
  public static function actionHideDetails()
  {
    return array_column(self::LIST, 'is_hide_detail', 'action');
  }

  //判断当前状态是否需要回滚提交处理的数据
  public static function istableDataSave($info)
  {
    if (!isset($info['status']) || $info['status'] == 2) {
      return true;
    }
    //草稿/撤回
    if (in_array($info['status'], [1, 6])) {
      return false;
    }
    $step_id = $info['step_id'] ?? ($info['next_step_id'] ?? 0);
    //4:驳回，当$step_id == 0时，驳回到发起人（提交时状态），其余为驳回到中间步骤
    if ($info['status'] == 4 && !$step_id) {
      return false;
    }

    return true;
  }

  /**
   * 获取状态列表（多code筛选）
   * @param $codes
   * @param $filters 过滤状态
   * @param $appendIds 追加
   * @return array
   */
  public static function getStatusListForCodes($codes, $filterIds = null, $appendIds = [])
  {
    $resultArr = self::LIST;
    $arr = [];

    foreach ($resultArr as $key => $value) {
      if (!empty($appendIds) && in_array($value['id'], $appendIds)) {
        $arr[] = ['value' => $value['id'], 'label' => $value['title']];
        continue;
      }


      if (!empty($codes)) {
        $intersectCodes = array_intersect($value['code'], $codes);
      } else {
        $intersectCodes = $value['code'];
      }

      if (!empty($intersectCodes) &&
        (!is_array($filterIds) || !in_array($value['id'], $filterIds))
      ) { //过滤完成状态
        $arr[] = ['value' => $value['id'], 'label' => $value['title']];
      }
    }
    unset($resultArr);
    return $arr;
  }

  /**
   * 审批中状态id
   * @return array
   */
  public static function getAuditingIds(){
    $ids = array_column(self::LIST, 'id');
    $noAuditingIds = [1, 2, 4, 6];
    return array_diff($ids, $noAuditingIds);
  }
}
