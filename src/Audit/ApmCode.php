<?php
/**
 * ApmCode.php
 * Notes:
 * author: chen
 * DateTime: 2022/5/20 18:34
 * @package Xtsb\Cims\Audit
 */

namespace Xtsb\Cims\Audit;

use Xtsb\Cims\Exception\ApiErrorDesc;
use Xtsb\Cims\Exception\ApiException;
class ApmCode
{

  const LIST = [

    //组织机构
    ['value' => 'branch_apply', 'label' => '分支机构签约/续约', 'sort' => 10],
    ['value' => 'branch_apply_break', 'label' => '分支机构解约', 'sort' => 19],


    //市场销售
    ['value' => 'sale_lead', 'label' => '销售线索', 'sort' => 11],
    ['value' => 'sale_platform', 'label' => '销售平台', 'sort' => 18],
    ['value' => 'sale_lead_track', 'label' => '拜访跟进', 'sort' => 12, 'phrase' => ['market']],
    ['value' => 'design_task', 'label' => '项目设计', 'sort' => 13, 'phrase' => ['market']],
    ['value' => 'bid', 'label' => '发起投标', 'sort' => 14, 'phrase' => ['market']],
    ['value' => 'bid_task', 'label' => '标书任务', 'sort' => 15, 'phrase' => ['market']],
    ['value' => 'cca', 'label' => '合同申请', 'sort' => 16, 'phrase' => ['market']],
    ['value' => 'cor', 'label' => '工程申请', 'sort' => 17, 'phrase' => ['market', 'construct', 'maintain']],
    ['value' => 'design_change', 'label' => '设计变更', 'sort' => 20, 'phrase' => ['construct', 'maintain']],
    ['value' => 'finance_schedule_income', 'label' => '回款计划', 'sort' => 32],

    //工程项目跟踪 - 工程准备阶段
    ['value' => 'team_protocol', 'label' => '工程队合同申请', 'sort' => 21, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_team_assign', 'label' => '派单申请', 'sort' => 22, 'phrase' => ['construct', 'maintain']],
    ['value' => 'goods_purchase_apply', 'label' => '采购申请', 'sort' => 23, 'phrase' => ['construct', 'maintain']],
    ['value' => 'goods_purchase', 'label' => '采购订单', 'sort' => 24, 'phrase' => ['construct', 'maintain']],
    ['value' => 'goods_purchase_pay', 'label' => '采购付款', 'sort' => 25, 'phrase' => ['construct', 'maintain']],
    ['value' => 'goods_purchase_donor', 'label' => '甲供领料', 'sort' => 26, 'phrase' => ['construct', 'maintain']],
    ['value' => 'goods_pick_cor', 'label' => '领料出库', 'sort' => 28, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_goods_pick', 'label' => '工程队领料', 'sort' => 29, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_ledger', 'label' => '工程内容申请', 'sort' => 30, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_ledger_decide', 'label' => '立项关联', 'sort' => 31, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_cancel', 'label' => '项目取消', 'sort' => 33, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_recover', 'label' => '项目恢复', 'sort' => 34, 'phrase' => ['construct', 'maintain']],

    //工程整体过程管理
    ['value' => 'cor_report_weekly', 'label' => '工程周报', 'sort' => 35, 'phrase' => ['construct', 'maintain']],
    ['value' => 'safety_inspect', 'label' => '安全检查', 'sort' => 36, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_inspect_qc', 'label' => '质检申请', 'sort' => 38, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_accept_material', 'label' => '验收资料审核', 'system' => '', 'sort' => 39, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_audit_material', 'label' => '送审资料审核', 'system' => '', 'sort' => 40, 'phrase' => ['construct', 'maintain']],

    // 维护合同
    ['value' => 'maintain_cca', 'label' => '维护合同申请', 'sort' => 41],
    ['value' => 'maintain_cor', 'label' => '维护订单申请', 'sort' => 42],

    //工程材料平衡
    ['value' => 'goods_return_cor', 'label' => '余料退库', 'sort' => 50, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_goods_return', 'label' => '工程队退料', 'sort' => 51, 'phrase' => ['construct', 'maintain']],
    ['value' => 'goods_purchase_donor_return', 'label' => '甲供退料', 'sort' => 52, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_ledger_adjust', 'label' => '期末台账平衡', 'sort' => 53, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_ledger_pay', 'label' => '工程队平衡缴款', 'sort' => 54, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_ledger_return', 'label' => '工程队平衡退库', 'sort' => 55, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_ledger_verify', 'label' => '工单核销', 'sort' => 55, 'phrase' => ['construct', 'maintain']],

    //工程结算
    ['value' => 'finance_settlement', 'label' => '下游支付', 'sort' => 72, 'phrase' => ['construct', 'maintain']],
    ['value' => 'finance_settlement_invoice', 'label' => '发票结算', 'sort' => 72, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_settlement', 'label' => '工程队结算', 'sort' => 73, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_settlement_pay', 'label' => '工程队结算付款', 'sort' => 74, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_salary', 'label' => '工人工资发放', 'sort' => 75, 'phrase' => ['construct', 'maintain']],
    ['value' => 'ca', 'label' => '项目费用', 'sort' => 76, 'phrase' => ['market']],
    ['value' => 'finance_settlement_pay', 'label' => '发票结算支付', 'sort' => 77, 'phrase' => ['construct', 'maintain']],
//    ['value' => 'finance_settlement_adjust', 'label' => '结算调整', 'sort' => 78, 'phrase' => ['construct', 'maintain']],
    ['value' => 'team_borrow', 'label' => '工程队借款', 'sort' => 79],

    //工程收入
    ['value' => 'project_audit', 'label' => '送审申请', 'sort' => 81, 'phrase' => ['construct', 'maintain']],
    ['value' => 'finance_invoice', 'label' => '发票开具', 'sort' => 82, 'phrase' => ['construct', 'maintain']],
    ['value' => 'finance_cost_invoice', 'label' => '结算进项票补录', 'sort' => 82, 'phrase' => ['construct', 'maintain']],
    ['value' => 'finance_income', 'label' => '到款通知', 'sort' => 83, 'phrase' => ['construct', 'maintain']],
    ['value' => 'exchange_income', 'label' => '其他类型到款确认', 'sort' => 84],
    ['value' => 'repayment_income', 'label' => '还款确认', 'sort' => 85],
    //工程项目细节跟进 - 工程内容
    ['value' => 'goods_supplier', 'label' => '供应商新增', 'sort' => 91],
    ['value' => 'project_start', 'label' => '开工申请', 'sort' => 93, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_complete', 'label' => '完工申请', 'sort' => 94, 'phrase' => ['construct', 'maintain']],
    ['value' => 'project_inspect_sampling', 'label' => '抽检登记', 'sort' => 95],

    //工单跟踪
    ['value' => 'project_image', 'label' => '关键点上报', 'sort' => 96],
    ['value' => 'project_report', 'label' => '施工报告', 'sort' => 97],
    ['value' => 'work_safety_check', 'label' => '施工安全检查', 'sort' => 98],

    //仓储
    ['value' => 'inventory_in', 'label' => '入库制单', 'sort' => 101],
    ['value' => 'inventory_out', 'label' => '出库制单', 'sort' => 102],
    ['value' => 'inventory_check', 'label' => '库存盘点', 'sort' => 103],

    ['value' => 'finance_invoice_invalid', 'label' => '发票作废', 'sort' => 111],
    ['value' => 'finance_cost_invoice_invalid', 'label' => '进项票作废', 'sort' => 112],
    ['value' => 'finance_schedule', 'label' => '资金拨付计划', 'sort' => 113],
    ['value' => 'rode', 'label' => '日常费用', 'sort' => 115],
    ['value' => 'my_imprest', 'label' => '备用金申请', 'sort' => 116],
    ['value' => 'seal', 'label' => '用印申请', 'sort' => 117],
    ['value' => 'download', 'label' => '资料下载申请', 'sort' => 118],
    ['value' => 'rewards', 'label' => '奖惩申请', 'sort' => 119],
    ['value' => 'personnel', 'label' => '人事申请', 'sort' => 120],
    ['value' => 'attendance_record', 'label' => '休假出勤', 'sort' => 121],
    ['value' => 'staff_salary', 'label' => '员工工资发放', 'sort' => 122],
    ['value' => 'company_salary', 'label' => '保险缴纳', 'sort' => 123],
    ['value' => 'finance_tax_certificate', 'label' => '外管证台账', 'sort' => 123],
    ['value' => 'finance_tax_certificate_invalid', 'label' => '外管证台账作废', 'sort' => 124],
    ['value' => 'minor_purchase', 'label' => '零星采购费用申请', 'sort' => 125],
    ['value' => 'project_advance_pay', 'label' => '工程预付款', 'sort' => 126],

    ['value' => 'sale_combine', 'label' => '销售套装', 'sort' => 141],

    ['value' => 'asset_dispose', 'label' => '资产处置', 'sort' => 131],
    ['value' => 'asset_transfer', 'label' => '资产移交', 'sort' => 132],
    ['value' => 'asset_use', 'label' => '资产使用', 'sort' => 134],
    ['value' => 'asset_upkeep', 'label' => '资产保养', 'sort' => 135],
    ['value' => 'asset_repair', 'label' => '资产维修', 'sort' => 136],
    ['value' => 'oil_card_record', 'label' => '油卡充值', 'sort' => 133],

    ['value' => 'report_weekly', 'label' => '工作周报', 'sort' => 165],
    ['value' => 'report_monthly', 'label' => '工作月报', 'sort' => 166],


    ['value' => 'sub_cca', 'label' => '下游合同', 'sort' => 167],
    ['value' => 'sub_cor_model', 'label' => '下游合同范本', 'sort' => 169],
    ['value' => 'sub_cor', 'label' => '下游订单', 'sort' => 168],

    ['value' => 'file_apply', 'label' => '合同原件申请', 'sort' => 170],

//    ['value' => 'other_company_goods_borrow', 'label' => '借料申请', 'sort' => 171],
//    ['value' => 'other_company_goods_return', 'label' => '还料申请', 'sort' => 172],
    ['value' => 'rome', 'label' => '市场费用', 'sort' => 173],
    ['value' => 'project_work_plan', 'label' => '施工计划', 'sort' => 174],

    ['value' => 'news', 'label' => '通知公告', 'sort' => 177],
    ['value' => 'safety_access', 'label' => '考核处理', 'sort' => 178],
    ['value' => 'safety_rectify_notice', 'label' => '安全整改通知', 'sort' => 180],

    ['value' => 'staff_status_up', 'label' => '转正申请', 'sort' => 181],
    ['value' => 'staff_status_off', 'label' => '离职申请', 'sort' => 182],

    ['value' => 'worker_contract', 'label' => '劳务合同申请', 'sort' => 183],

    ['value' => 'company_goods_borrow', 'label' => '材料调拨', 'sort' => 184],
    ['value' => 'company_goods_return', 'label' => '调拨还料', 'sort' => 185],

    ['value' => 'attendance_overwork', 'label' => '加班申请', 'sort' => 186],
    ['value' => 'attendance_supply', 'label' => '补卡申请', 'sort' => 187],

    ['value' => 'project_goods_balance', 'label' => '平衡材料', 'sort' => 190],

    ['value' => 'attendance_report_by_week', 'label' => '周报', 'sort' => 199],

    ['value' => 'file_aver', 'label' => '资料归档', 'sort' => 200],

    //生产物料
    ['value' => 'manufacture_goods_purchase', 'label' => '生产物料采购订单', 'sort' => 211, 'phrase' => ['construct', 'maintain']],

    ['value' => 'mes_sale_order', 'label' => '销售订单', 'sort' => 212],
    ['value' => 'company_risk_detect', 'label' => '税务风险检测', 'sort' => 213],

    ['value' => 'goods_supplier_apply', 'label' => '往来单位申请', 'sort' => 214],

    ['value' => 'file_apply_credit', 'label' => '资质原件申请', 'sort' => 215],
  ];


  public static function list()
  {
    //开票结算， 有开关控制， 开启时，才可以设置以下流程： 发票成本票补录， 发票结算、结算付款
    $finance_invoice_settlement = FinanceInvoiceServer::getInvoiceConfig('is_settlement');
    if ($finance_invoice_settlement){
      $exclude_list =  ['attendance_report_by_week'];
    }else {
      $exclude_list =  ['finance_cost_invoice', 'finance_settlement_invoice', 'finance_settlement_pay','attendance_report_by_week'];
    }

    $list = [];
    foreach (self::LIST as $item) {
      if (!in_array($item['value'], $exclude_list)){
        $list[] = $item;
      }
    }
    return $list;
  }

  public static function apmNames()
  {
    return array_column(self::LIST, 'label', 'value');
  }

  public static function apmCodes()
  {
    return array_column(self::LIST, 'value', 'label');
  }

  //首页，用于获取筛选栏的流程类型的下拉列表
  public static function getTableNameFilterList($table_name_array, $table_name_list = null)
  {
    $list = [];
    if (!$table_name_list) {
      $table_name_list = self::apmNames();
    }

    if (!empty($table_name_array)) {
      foreach ($table_name_array as $item) {
        $list[] = ['value' => $item['table_name'], 'label' => $table_name_list[$item['table_name']] ?? $item['table_name']];
      }
    }
    $list = \app\Http\OA\Servers\IndexServer::uniqueMultiArray($list);
    return $list;
  }

  public static function getCostTableNameFilterList($table_name_array)
  {

  }

  //首页，用于获取流程审批的流程类型+流程名称
  public static function getAmpName(&$item, $apm_list = null)
  {

    if (!$apm_list)
      $apm_list = self::apmCodes();
    $apm_type = $apm_list[$item['apm_code'] ?? $item['table_name']] ?? $item['table_name'];
    if (!$item['apm_name'])
      $item['apm_name'] = $apm_type;
    else {
      if ($apm_type != $item['apm_name']) {
        $item['apm_name'] = $apm_type . '/' . $item['apm_name'];
      }
    }
  }

  ##根据控制器， 获取对应的流程名称
  public static function getApmLabel($table_name){
    $label =  self::apmNames()[$table_name]??null;
    if(empty($label)){
      if($table_name=='my_cost'){
        $label = '费用申请/报销';
      }else{
        throw new ApiException(ApiErrorDesc::ERROR_DEFAULT, '数据需要适配，请联系平台管理员');
      }
    }
    return $label;
  }
}
