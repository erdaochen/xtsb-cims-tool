<?php


namespace Xtsb\Cims\Ocr\Custom;


class Invoice
{
  static function data(){
    return '
      "algo_version": "5d7591fa64b2e4a91d08b0743c22147c34558db2",
			"angle": 0,
			"data": {
				"发票代码": "035021700107",
				"发票号码": "04822552",
				"发票详单": [{
					"单价": "8.63",
					"数量": "69.53",
					"金额": "1400.400",
					"项目": "*汽油*95#车用汽油(VD)"
				}],
				"合计金额(大写)": "陆佰圆整",
				"合计金额(小写)": "600.00",
				"开票日期": "2023-09-0",
				"收款员": "凌宏盛",
				"机器编号": "49949445466",
				"机打号码": "0412242",
				"标题": "",
				"校验码": "04636437488816018231",
				"购买方名称": "信连保(厦门)信息科技有限公司",
				"购买方税号": "91350211MA3114EP411X",
				"销售方名称": "国(厦门)石油制品有限公司",
				"销售方税号": "913502065254009126"
    ';
  }

  static function struct($data){
    $list = $data['发票详单']??[];
    $data = [
      'supplier_name'=>$data['销售方名称']??null,
      'seller_info'=>[
        'name'=>$data['销售方名称']??null,
        'bank'=>'',
        'address'=>'',
        'tax_no'=>$data['销售方税号']??null,
        'tax_rate'=>null,
      ],
      'amount_all'=>$data['合计金额(小写)']??"",
      'not_tax_money_all'=>0,
      'tax_money_all'=>0,
      'invoice_date'=>$data['开票日期']??"",

      'purchase_name'=>$data['购买方名称']??null,
      'purchase_info'=>[
        'name'=>$data['购买方名称']??null,
        'bank'=>'',
        'address'=>'',
        'tax_no'=>$data['购买方税号']??'',
      ],

      'invoice_order_no'=>$data['发票号码']??null,
      'code'=>$data['发票代码']??null,
      'invoice_type'=>1,
      'invoice_org'=>null,
      'machine_code'=>$data['机打号码']??null,
      'check_code'=>$data['校验码']??null,
      'invoice_list' =>[]
    ];

    foreach ($list as $item){
      $data['invoice_list'][] = [
        'row_num'=>$item['数量'],
        'content'=>$item['项目']??"",
        'sku'=>'',
        'unit'=>'',
        'num'=>$item['数量'],
        'price'=>$item['单价'],
        'amount'=>$item['金额'],
        'tax_rate'=>null,
        'tax_money'=>null,
        'not_tax_money'=>null
      ];
    }
    return $data;
  }
  
}