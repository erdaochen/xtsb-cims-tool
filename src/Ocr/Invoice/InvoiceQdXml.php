<?php

namespace Xtsb\Cims\Ocr\Invoice;

class InvoiceQdXml
{
  static function parseByXML($object){
    $object = json_decode(json_encode($object),true);
    $data = $object['EInvoiceData']??[];
    $sale = $data['SellerInformation']??[];
    $buyer = $data['BuyerInformation']??[];
    $goods = $data['IssuItemInformation']??[];
    $basic = $data['BasicInformation']??[];
    $header = $object['Header'];
    $invoice_type = $header['GeneralOrSpecialVAT']['LabelName']??"发票";
    $invoice_type = strstr($invoice_type,'专用')?2:1;
    $invoice_list = [];
    $super = $object['TaxSupervisionInfo'];

    $tax_rate = "";
    $invoice = [];
    $kv = [
      'ItemName'=>'content','MeaUnits'=>'unit','Quantity'=>'num','UnPrice'=>'price',
      'TotaltaxIncludedAmount'=>'amount','ComTaxAm'=>'tax_money','Amount'=>'not_tax_money'
    ];
    foreach ($goods as $key=>$item){
      if(is_int($key)){
        $tax_rate = ( $item['TaxRate']??"0.01") * 100;
        $invoice_list[] = [
          'row_num'=>$key,
          'content'=>$item['ItemName']??null,
          'sku'=>null,
          'unit'=>$item['MeaUnits']??null,
          'num'=>(float)($item['Quantity']??null),
          'price'=>(float)($item['UnPrice']??null),
          'amount'=>(float)($item['TotaltaxIncludedAmount']??0),
          'tax_rate'=>$tax_rate,
          'tax_money'=>(float)($item['ComTaxAm']??0),
          'not_tax_money'=>(float)($item['Amount']??0),
        ];
      }else{
        $index = $kv[$key]??"row_num";
        $invoice[$index] = $item;
      }
    }

    if($invoice){
      $invoice['row_num'] = count($invoice_list);
      $invoice_list[] = $invoice;
    }

    $data = [
      'supplier_name'=>$sale['SellerName']??null,
      'seller_info'=>[
        'name'=>$sale['SellerName']??null,
        'tax_no'=>$sale['SellerBankAccNum']??null,
        'bank'=>($sale['SellerName']??null).' '.($sale['SellerBankName']??null),
        'address'=>($sale['SellerName']??null).' '.($sale['SellerAddr']??null).' '.($sale['SellerTelNum']??null),
        'tax_rate'=>$tax_rate??null,
      ],
      'amount_all'=>str_replace('￥','', $basic['TotalTax-includedAmount']??0),
      'not_tax_money_all'=>str_replace('￥','', $basic['TotalAmWithoutTax']??0),
      'tax_money_all'=>str_replace('￥','', $basic['TotalTaxAm']??0),
      'invoice_date'=>self::replaceInvoiceDate($super['IssueTime']??""),
      'purchase_name'=>$buyer['BuyerName']??null,
      'purchase_info'=>[
        'name'=>$buyer['BuyerName']??null,
        'tax_no'=>$buyer['BuyerIdNum']??null,
        'bank'=>($buyer['BuyerName']??null).' '.($buyer['BuyerBankName']??null),
        'address'=>($buyer['BuyerName']??null).' '.($buyer['BuyerAddr']??null).' '.($buyer['BuyerTelNum']??null),
      ],
      'invoice_order_no'=>$super['InvoiceNumber']??null,
      'code'=>$header['EInvoiceTag']??null,
      'invoice_type'=>strstr($invoice_type,'专用')?1:2,
      'invoice_org'=>$super['TaxBureauName']??null,
      'machine_code'=>$header['EInvoiceTag']??null,
      'check_code'=>$super['TaxBureauCode']??null,
      'invoice_list' =>$invoice_list
    ];
    return $data;
  }

  static function replaceInvoiceDate($date){
    $date = str_replace('年','-',$date);
    $date = str_replace('月','-',$date);
    return  str_replace('日','',$date);
  }
}