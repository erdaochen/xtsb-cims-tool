<?php


namespace Xtsb\Cims\Ocr\Invoice;


/**图片识别
 * Class InvoiceImgOcr
 * @package app\Ocr
 */
class InvoiceImgOcr extends BaseOcr
{

  static $request_url = "https://vatinvoice.market.alicloudapi.com/ai_vat_invoice";
  
  static function request($save_name){
    //http://filesvr.chetell.com/36.137.22.163/test/temp/finance_invoice/UID_294/%E5%8F%91%E7%A5%A8%E9%99%84%E4%BB%B6/1723.00%20%E4%BD%8F%E5%AE%BF%E8%B4%B9___20230731105638.jpg
    //http://filesvr.chetell.com/36.137.22.163/test/temp/finance_invoice/UID_294/发票附件/1723.00+%E4%BD%8F%E5%AE%BF%E8%B4%B9___20230731105638.jpg
    //http://filesvr.chetell.com/36.137.22.163/test%2Ftemp%2Ffinance_invoice%2FUID_294%2F%E5%8F%91%E7%A5%A8%E9%99%84%E4%BB%B6/1723.00%20%E4%BD%8F%E5%AE%BF%E8%B4%B9___20230731105638.jpg
    $save_name = str_replace('+', '%20', $save_name);
    $options = [
      'form_params' => [
        'AI_VAT_INVOICE_IMAGE_TYPE' => '1',
        'AI_VAT_INVOICE_IMAGE' => $save_name,
      ]
    ];
    $body = self::getRequest($options,'POST');
    return self::parse($body);
  }

  static function parse($response){
    $object = json_decode($response,true);
    $date = $object['INVOICE_TIME']??"";
    $type = $object['INVOICE_TYPE']??"";
    $is_special = strstr($type,'专票')?:"普票";
    $goods = $object['INVOICE_DETAIL']??[];
    $first_goods = $goods[0]??[];
    $invoice_id = $object['INVOICE_ID']??"";
    //$invoice_code = $object['INVOICE_CODE']??"";
    $data = [
      'code'=>$object['INVOICE_MACHINE_CODE']??"",
      'invoice_date'=>self::replaceInvoiceDate($date),
      'order_no'=>$invoice_id,
      'invoice_type'=>$is_special?1:2,
      'tax_rate'=>self::replaceTaxRate($first_goods['ITEM_PRODUCT_TAX_RATE']??""),

      'content'=>$first_goods['ITEM_PRODUCT_NAME']??"",
      'sku'=>$first_goods['ITEM_PRODUCT_TYPE']??"",
      'unit'=>$first_goods['ITEM_PRODUCT_UNIT']??"",
      'num'=>$first_goods['ITEM_ROW_NUMBER']??"",

      'amount'=>$object['INVOICE_TOTAL_AMOUNT_AND_TAX_SMALL']??"",
      'not_tax_money'=>$object['INVOICE_TOTAL_AMOUNT']??"",
      'tax_money'=>$object['INVOICE_TOTAL_TAX']??"",
      'supplier_name'=>$object['INVOICE_SELLER_NAME']??'',
    ];
    return $data;
  }


  static function requestNew($save_name){
    //http://filesvr.chetell.com/36.137.22.163/test/temp/finance_invoice/UID_294/%E5%8F%91%E7%A5%A8%E9%99%84%E4%BB%B6/1723.00%20%E4%BD%8F%E5%AE%BF%E8%B4%B9___20230731105638.jpg
    //http://filesvr.chetell.com/36.137.22.163/test/temp/finance_invoice/UID_294/发票附件/1723.00+%E4%BD%8F%E5%AE%BF%E8%B4%B9___20230731105638.jpg
    //http://filesvr.chetell.com/36.137.22.163/test%2Ftemp%2Ffinance_invoice%2FUID_294%2F%E5%8F%91%E7%A5%A8%E9%99%84%E4%BB%B6/1723.00%20%E4%BD%8F%E5%AE%BF%E8%B4%B9___20230731105638.jpg
    $save_name = str_replace('+', '%20', $save_name);
    $options = [
      'form_params' => [
        'AI_VAT_INVOICE_IMAGE_TYPE' => '1',
        'AI_VAT_INVOICE_IMAGE' => $save_name,
      ]
    ];
    $body = self::getRequest($options,'POST');
    return self::parseNew($body);
  }

  static function parseNew($response){
    $object = json_decode($response,true);
    $data = [
      'supplier_name'=>$object['INVOICE_SELLER_NAME']??null,
      'seller_info'=>[
        'name'=>$object['INVOICE_SELLER_NAME']??'',
        'bank'=>$object['INVOICE_SELLER_BANK']??'',
        'address'=>$object['INVOICE_SELLER_ADDRESS']??'',
        'tax_no'=>$object['INVOICE_SELLER_REGISTER_ID']??'',
        'tax_rate'=>self::replaceTaxRate($object['INVOICE_SELLER_TAX_RATE']??null),
      ],
      'amount_all'=>$object['INVOICE_TOTAL_AMOUNT_AND_TAX_SMALL']??0,
      'not_tax_money_all'=>$object['INVOICE_TOTAL_AMOUNT']??0,
      'tax_money_all'=>$object['INVOICE_TOTAL_TAX']??0,
      'invoice_date'=>self::replaceInvoiceDate($object['INVOICE_TIME']??""),
      'purchase_name'=>$object['INVOICE_PURCHASER_NAME']??null,
      'purchase_info'=>[
        'name'=>$object['INVOICE_PURCHASER_NAME']??null,
        'bank'=>$object['INVOICE_PURCHASER_BANK']??'',
        'address'=>$object['INVOICE_PURCHASER_ADDRESS']??'',
        'tax_no'=>$object['INVOICE_PURCHASER_REGISTER_ID']??'',
      ],
      'invoice_order_no'=>$object['INVOICE_ID']??null,
      'code'=>$object['INVOICE_CODE']??null,
      'invoice_type'=>strstr($object['INVOICE_TYPE']??"",'专用发票')?1:2,
      'invoice_org'=>$object['INVOICE_TYPE_ORG']??null,
      'machine_code'=>$object['INVOICE_MACHINE_CODE']??null,
      'check_code'=>$object['INVOICE_CHECKED_CODE']??null,
      'invoice_list' =>[]
    ];
    if(!empty($object['INVOICE_DETAIL'])){
      foreach ($object['INVOICE_DETAIL'] as $item){
        $data['invoice_list'][] = [
          'row_num'=>$item['ITEM_ROW_NUMBER'],
          'content'=>$item['ITEM_PRODUCT_NAME'],
          'sku'=>$item['ITEM_PRODUCT_TYPE'],
          'unit'=>$item['ITEM_PRODUCT_UNIT'],
          'num'=>$item['ITEM_PRODUCT_AMOUNT'],
          'price'=>$item['ITEM_PRODUCT_PRICE'],
          'amount'=>(float)$item['ITEM_PRODUCT_SUM_MONEY']+(float)$item['ITEM_PRODUCT_SUM_TAX'],
          'tax_rate'=>self::replaceTaxRate($item['ITEM_PRODUCT_TAX_RATE']??null),
          'tax_money'=>$item['ITEM_PRODUCT_SUM_TAX'],
          'not_tax_money'=>$item['ITEM_PRODUCT_SUM_MONEY'],
        ];
      }
    }
    return $data;
  }
}