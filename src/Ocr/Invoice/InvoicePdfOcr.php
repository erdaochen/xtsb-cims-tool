<?php


namespace Xtsb\Cims\Ocr\Invoice;

/**pdf识别
 * Class InvoicePdfOcr
 * @package app\Ocr
 */
class InvoicePdfOcr extends BaseOcr
{
  static $request_url = "http://vatinvoice.market.alicloudapi.com/ai_market/ocr/digital_invoice/pdf/v1";
  
  static function request($save_name){
    $save_name = str_replace(".PDF",'.pdf',$save_name);
    $options = [
      'form_params' => [
        'URL' => $save_name,
      ]
    ];
    $body = self::getRequest($options);
    $data = self::parse($body);
    return $data;
  }

  static $notice = '';
  static function parse($response){
    $json = json_decode($response,true);
    $object = $json['增值税电子发票实体信息']??[];
    self::$notice = $json['增值税电子发票增识别状态']??"";
    $goods = $object['货物或应税劳务、服务名称或项目名称实体信息']??[];
    $first_goods =  $goods[0]??[];
    $invoice_type = $object['发票名称']??"";
    $is_special = strstr($invoice_type,'专票')?:strstr($invoice_type,'专用发票');
    $date = $object['开票日期']??"";
    $sale = $object['销售方']??[];
    $data = [
      'code'=>$object['发票代码']??"",
      'invoice_date'=>self::replaceInvoiceDate($date),
      'order_no'=>$object['发票号码']??"",
      'invoice_type'=>$is_special?1:2,
      'tax_rate'=>self::replaceTaxRate($first_goods['税率']??""),
      'content'=>$first_goods['货物或应税劳务、服务名称或项目名称']??"",
      'sku'=>$first_goods['规格型号']??"",

      'unit'=>$first_goods['单位']??"",
      'num'=>$first_goods['数量']??"",

      'amount'=>$object['价税合计（小写）']??"",
      'not_tax_money'=>$object['合计（金额）']??"",
      'tax_money'=>$object['合计（税额）']??"",
      'supplier_name'=>$sale['名称']??''
    ];
    return $data;
  }

  static function requestNew($save_name){
    $save_name = str_replace(".PDF",'.pdf',$save_name);
    $options = [
      'form_params' => [
        'URL' => $save_name,
      ]
    ];
    $body = self::getRequest($options);
    return self::parseNew($body);
  }

  static function parseNew($response){
    $json = json_decode($response,true);
    $object = $json['增值税电子发票实体信息']??[];
    $data = [
      'supplier_name'=>empty($object['销售方'])?null:($object['销售方']['名称']??null),
      'seller_info'=>[
        'name'=>empty($object['销售方'])?null:($object['销售方']['名称']??null),
        'bank'=>empty($object['销售方'])?null:($object['销售方']['开户行及账号']??null),
        'address'=>empty($object['销售方'])?null:($object['销售方']['地址、电话']??null),
        'tax_no'=>empty($object['销售方'])?null:($object['销售方']['纳税人识别号']??null),
        'tax_rate'=>self::replaceTaxRate($object['销售方']['企业税率']??null),
      ],
      'amount_all'=>str_replace('￥','', $object['价税合计（小写）']??0),
      'not_tax_money_all'=>str_replace('￥','', $object['合计（金额）']??0),
      'tax_money_all'=>str_replace('￥','', $object['合计（税额）']??0),
      'invoice_date'=>self::replaceInvoiceDate($object['开票日期']??""),
      'purchase_name'=>empty($object['购买方'])?null:($object['购买方']['名称']??null),
      'purchase_info'=>[
        'name'=>empty($object['购买方'])?null:($object['购买方']['名称']??null),
        'bank'=>empty($object['购买方'])?null:($object['购买方']['开户行及账号']??null),
        'address'=>empty($object['购买方'])?null:($object['购买方']['地址、电话']??null),
        'tax_no'=>empty($object['购买方'])?null:($object['购买方']['纳税人识别号']??null),
      ],
      'invoice_order_no'=>$object['发票号码']??null,
      'code'=>$object['发票代码']??null,
      'invoice_type'=>strstr($object['发票名称']??"",'专用发票')?1:2,
      'invoice_org'=>$object['发票名称']??null,
      'machine_code'=>$object['机器编号']??null,
      'check_code'=>$object['校验码']??null,
      'invoice_list' =>[]
    ];

    if(!empty($object['货物或应税劳务、服务名称或项目名称实体信息'])){
      foreach ($object['货物或应税劳务、服务名称或项目名称实体信息'] as $key=>$item){
        $data['invoice_list'][] = [
          'row_num'=>$key,
          'content'=>$item['货物或应税劳务、服务名称或项目名称']??null,
          'sku'=>$item['规格型号']??null,
          'unit'=>$item['单位']??null,
          'num'=>$item['数量']??null,
          'price'=>$item['单价']??null,
          'amount'=>(float)($item['金额']??0)+(float)($item['税额']??0),
          'tax_rate'=>self::replaceTaxRate($item['税率']??null),
          'tax_money'=>(float)($item['税额']??0),
          'not_tax_money'=>(float)($item['金额']??0),
        ];
      }
    }
    return $data;
  }
  
}