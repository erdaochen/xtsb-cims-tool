<?php


namespace Xtsb\Cims\Excel;

class Cell
{
  private static $instance;
  protected static $style;

  private function __construct()
  {
  }

  public static function getInstance()
  {
    if (!self::$instance instanceof self) {
      self::$instance = new self();
    }
    self::$style = ['value' => '', 'style' => ['colNum' => 1, 'rowNum' => 1, 'colWidth' => 20]];
    return self::$instance;
  }

  /**大标题
   * @param $value
   * @param $colNum
   * @return mixed
   */
  public function title($value,$colNum=11)
  {
    return $this->width(20,$colNum)->size(18)->color("ED7D31")->set($value);
  }

  /**页眉
   * @param $value
   * @param int $width
   * @return mixed
   */
  public function head($value,$width=20)
  {
    return $this->width($width)->color()->set($value);
  }

  /**宽度设置
   * @param int $colNum
   * @param int $rowNum
   * @param int $colWidth
   * @return $this
   */
  public function width($colWidth = 20,$colNum = 1, $rowNum = 1)
  {
    self::$style['style']['colNum'] = $colNum;
    self::$style['style']['rowNum'] = $rowNum;
    self::$style['style']['colWidth'] = $colWidth;
    return $this;
  }

  /**颜色设置
   * @param string $bgColor  blue:00B0F0  orange:ED7D31   red
   * @param int $fontBold
   * @return $this
   */
  public function color($bgColor = '00B0F0', $fontBold = 2)
  {
    self::$style['style']['fontBold'] = $fontBold;
    self::$style['style']['bgColor'] = $bgColor;
    return $this;
  }

  public function size($fontSize=15){
    self::$style['style']['fontSize'] = $fontSize;
    return $this;
  }

  /**设置标题
   * @param $value
   * @return mixed
   */
  public function set($value)
  {
    self::$style['value'] = $value;
    return self::$style;
  }

  /**内容组装
   * @param $list
   * @param $keyArray
   * @return array
   */
  public function getBody($list,$keyArray){
    $data = [];
    foreach ($list as $key => $value) {
      foreach ($keyArray as $k => $val) {
        if ($val == 'sn') {
          $data[$key][$k]['value'] = $key + 1;
          $data[$key][$k]['format'] = 'int';
        } else {
          $data[$key][$k]['value'] = $value[$val];
        }
      }
    }
    return $data;
  }

  /**获取文件名
   * @param $fileName
   * @return string
   */
  public function getFilename($fileName){
    return  $fileName.'_' . date('YmdHis') . '.xlsx';
  }

  /**获取输出对象
   * @param $header
   * @param $body
   * @param $filename
   * @return array
   */
  public function getResponse($header,$body,$filename){
    $datas = array_merge($header,$body);
    $result = ['data' => $datas, 'filename' =>$filename];
    return ExcelFactory::export($result);
  }
}
