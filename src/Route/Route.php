<?php
/**
 * Rule.php
 * Notes:
 * author: chen
 * DateTime: 2022/8/29 19:20
 * @package Xtsb\Cims\Rule
 */

namespace Xtsb\Cims\Route;

use Xtsb\Cims\Exception\ApiErrorDesc;
use Xtsb\Cims\Exception\ApiException;

class Route
{

  protected static $rules;

  public static function instance($name): void
  {
    $rule = Request()->rule()->getRouter()->getRuleName()->getGroup($name);
    if (!isset($rule)) {
      throw new ApiException(ApiErrorDesc::ROUTE_NO_EXIST);
    }

    self::$rules = explode('\\', $rule->getRoute());
    unset($rule);
  }


  public static function getController($name)
  {
    return str_replace(" ", "", ucwords(str_replace('_', " ", $name)));
  }

  public static function getModule($name)
  {
    self::instance($name);
    return self::$rules[2];
  }

  public static function getAction($name)
  {
    return lcfirst(str_replace(" ", "", ucwords(str_replace('_', " ", $name))));
  }
}
