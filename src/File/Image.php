<?php

namespace Xtsb\Cims\File;

use Xtsb\Cims\Exception\ApiErrorDesc;
use Xtsb\Cims\Exception\ApiException;
use think\Exception;

class Image
{

  /**
   * 裁剪
   * @param $filepath string 文件路径 './test.png'
   * @param $size array 尺寸
   * @return string
   */
  public static function crop(string $filepath, array $size = [300, 300]): string
  {
    $image = \think\Image::open($filepath);
    // 返回图片的宽度
//    $width = $image->width();
    // 返回图片的高度
//    $height = $image->height();
    // 返回图片的类型
//    $type = $image->type();
    // 返回图片的mime类型
//    $mime = $image->mime();
    // 返回图片的尺寸数组 0 图片宽度 1 图片高度
//    $size = $image->size();

    //将图片裁剪为300x300并保存为crop.png
    $image->crop($size[0], $size[1])->save($filepath);

    return $filepath;
  }

  /**
   * 压缩/缩略图
   * @param $filepath string 文件路径 './test.png'
   * @param $replace bool 是否替换
   * @param $size array 尺寸
   * @return string
   */
  public static function thumb(string $filepath, bool $replace = false, array $size = [150, 150]): string
  {
    if (!is_file($filepath)) {
      throw new ApiException(ApiErrorDesc::ERROR_DEFAULT, '文件不存在');
    }

    $image = \think\Image::open($filepath);
    // 返回图片的宽度
//    $width = $image->width();
    // 返回图片的高度
//    $height = $image->height();
    // 返回图片的类型
    $type = $image->type();
    // 返回图片的mime类型
//    $mime = $image->mime();
    // 返回图片的尺寸数组 0 图片宽度 1 图片高度
//    $size = $image->size();

    $thumbFilepath = $filepath;
    if (!$replace) {
      //不替换原文件
      $filename = explode('.', basename($filepath))[0];
      $thumbFilepath = str_replace(basename($filepath), '', $filepath) . $filename . '_thumb.' . $type;
    }

    // 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.png
    $image->thumb($size[0], $size[1])->save($thumbFilepath);

    return $thumbFilepath;
  }

  /**
   * 添加水印
   * @param $filepath string './test.png'
   * @return string
   */
  public static function watermark(string $filepath, string $text = null,$savepath='test1.jpg'): string
  {
    $image = \think\Image::open($filepath);
    // 给原图左上角添加水印并保存water_image.png
//    $image->water('./logo.png', \think\Image::WATER_SOUTHEAST, 20)->save($filepath);

    // 给原图左上角添加水印并保存water_image.png #ffffff
    $image->text($text, './fonts/SimKai.ttf', 36, '#9e9a9a', \think\Image::WATER_CENTER, 0, 50)
      ->save($savepath);

    return $filepath;
  }

  /**
   * 创建文件
   * @param $imageFile
   * @param null $ip
   * @param string $msg
   * @return mixed|string
   */
  public static function changeImage($imageFile, $ip = null, $msg = '图片有误')
  {
    $fileInfo = pathinfo($imageFile);
    $rootPath = app_path().'../public/storage/change';
    if (!is_dir($rootPath)) {
      mkdir($rootPath);
    }

    $dirname = $fileInfo['dirname'];
    $oldFile = $rootPath . '/' . $fileInfo['basename'];
    file_put_contents($oldFile, file_get_contents(File::getFileUrl($imageFile, $ip)));

    $image = \think\Image::open($oldFile);
    $image->rotate(270)->save($oldFile);

    $newFileNames = File::postFile($oldFile, $dirname, $ip, 'file');
    if (empty($newFileNames[0])) {
      throw new ApiException(ApiErrorDesc::ERROR_DEFAULT, $msg);
    }

    if (is_file($oldFile)) {
      unlink($oldFile);
    }

    return $newFileNames[0];
  }


  static function isOutSize($url,$rule){
    $imageInfo = getimagesize($url);
    try{
      return $imageInfo[0] > $rule[0] || $imageInfo[1] > $rule[1];
    }catch (Exception $e){
      return false;
    }
  }

}