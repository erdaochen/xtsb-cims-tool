<?php
/**
 * Zip.php
 * Notes:文件压缩/解压
 * author: chen
 * DateTime: 2022/4/26 9:26
 * @package Xtsb\Cims\File
 */

namespace Xtsb\Cims\File;

class Zip
{
  /**
   * 压缩文件
   * @param array $files 待压缩文件 array('d:/test/1.txt'，'d:/test/2.jpg');【文件地址为绝对路径】
   * @param string $filePath 输出文件路径 【绝对文件地址】 如 d:/test/new.zip
   * @return string|bool
   */

  public static function zip($files, $filePath = './download/回单文件.zip')
  {
    //检查参数
    if (empty($files) || empty($filePath)) {
      return false;
    }

    //压缩文件
    $zip = new \ZipArchive();

    $zip->open($filePath, \ZipArchive::CREATE);

    foreach ($files as $key => $file) {

      //检查文件是否存在
      if (!file_exists($file)) {

        return false;
      }

      $zip->addFile($file, basename($file));
    }

    $zip->close();


    return true;

  }

  /**
   * 文件夹压缩
   * @param string $path 文件夹原路径
   * @param string $newPath 文件夹新路径
   * @param object $zip 压缩文件对象
   */
  public function addFileToZip($path, $newPath, $zip)
  {
    $handler = opendir($path); //打开当前文件夹由$path指定。
    while (($filename = readdir($handler)) !== false) {
      if ($filename != "." && $filename != "..") {//文件夹文件名字为'.'和‘..’，不要对他们进行操作
        if (is_dir($path . "/" . $filename)) {// 如果读取的某个对象是文件夹，则递归

          $this->addFileToZip($path . "/" . $filename, $newPath . "/" . $filename, $zip);

        } else { //将文件加入zip对象
          //合同、立项 资料

          $zip->addFile($path . "/" . $filename, isset($newPath) ? $newPath . "/" . $filename : null);
        }
      }
    }
//    halt($handler);
    @closedir($handler);
    return;
  }


  /**
   * zip解压方法
   * @param string $filePath 压缩包所在地址 【绝对文件地址】d:/test/123.zip
   * @param string $path 解压路径 【绝对文件目录路径】d:/test
   * @return bool
   */

  public static function unzip($filePath = './test.zip', $path = './zip')
  {
    if (empty($path) || empty($filePath)) {

      return false;
    }

    $zip = new \ZipArchive();

    if ($zip->open($filePath) === true) {

      $zip->extractTo($path);

      $zip->close();

      return true;
    } else {

      return false;
    }

  }


}
