<?php
/**
 * API 错误描述
 */

namespace Xtsb\Cims\Exception;

class ApiErrorDesc
{
  /**
   *
   * @OA\Schema(schema="task-model",type="object",title="Api-Error-Desc")
   */

  /**error info const array */

  /**
   * API 通用错误
   *
   * error_code < 1000
   */
  /**
   * 成功
   * @var array
   * @OA\Property(format="array", example=[0, 'SUCCESS'])
   */
  const SUCCESS = [0, 'SUCCESS'];
  const UNKNOW_ERROR = [1, '未知错误'];
  const ERROR_URL = [2, '访问的接口不存在'];

  const TOKEN_REQUIRE = [3, 'user-token不能为空'];
  const TOKEN_ERROR = [4, 'Token错误'];
  const TOKEN_EXPIRE = [5, 'Token过期'];
  const TOKEN_INVALID = [6, 'Token未生效'];

  const ROUTE_ERROR = [7, '路由配置错误'];

  const RULE_ERROR = [8, '没有权限'];

  const LOGIN_EXPIRE = [9, '登入超时'];

  const LOGIN_USER_ERROR = [10, '登入用户错误，请确认当前用户类型：企业用户，施工队用户'];

  const ERROR_PARAMS = [100, '参数错误'];

  const ERROR_DEFAULT = [101, ''];//自定义错误
  const ERROR_UNIQUE = [102, '参数存在'];//唯一性验证
  const ERROR_FORMAT = [103, '参数格式错误'];//格式验证

  const ERROR_MAIL = [104, '发送邮件失败，请联系平台客服'];//右键发送失败

  const SMS_COUNT_ERROR = [105, '获取验证码次数过多，已锁定,请于24小时后解锁'];//短信发送次数
  const SMS_REPEAT_ERROR = [106, '请勿重复请求，一分钟后再次获取短信验证码'];//短信发送重复请求
  const SMS_CODE_EXPIRE = [107, '验证码已过期'];//短信验证码过期
  const SMS_CODE_ERROR = [108, '验证码错误'];//短信验证码错误

  const ROUTE_NO_EXIST = [109, '路由未配置或未传入路由参数'];//路由未配置

  const DATA_NO_EXIST = [110, '数据不存在，请刷新页面重新加载数据'];//数据不存在

  const SYSTEM_ERROR = [111, '很抱歉！您所访问的页面已失联，请与我们的技术团队联系'];//
  const SYSTEM_UPDATE = [112, '系统已更新，请刷新页面使用最新系统'];//

  const SYSTEM_UPGRADE = [113, ''];//

  const APP_NOT_FINISH = [888, '项目还在开发中,请您移步到pc端...'];

  const EXCEPTION_ERROR = [999, '未知错误'];

  /**
   * error_code 1001-1100 用户登入相关错误码
   */
  const NULL_CUID = [1001, '企业账号不能为空'];
  const NULL_USERNAME = [1002, '用户账号不能为空'];
  const NULL_PASSWORD = [1003, '用户密码不能为空'];

  const UNKNOW_CUID = [1004, '企业不存在'];
  const ERROR_STATUS_CUID = [1005, '企业未激活'];
  const UNKNOW_USERNAME = [1006, '用户账号不存在'];
  const ERROR_STATUS = [1007, '用户账号已被禁用'];
  const ERROR_EXPIRES = [1008, '系统已到期，请续费'];
  const UNKNOW_PASSWORD = [1009, '密码输入错误'];
  const UN_STATUS_PAY_CUID = [1012, '【信钛精管云】尊敬的信钛客户，您好！贵司精管云-CIMS即将到期/已欠费，为保证正常使用，请及时续费/缴费以延长使用时间，现可享2年赠送2个月，3年可享9折优惠，5年可享8折优惠，详询官网客服400-9020-507或联系相应服务经理！'];
  const UN_STATUS_Test_CUID = [1013, '【信钛精管云】尊敬的信钛客户，您好！贵司精管云-CIMS试用即将到期，为保证正常使用，请及时缴费以延长使用时间，现可享2年赠送2个月，3年可享9折优惠，5年可享8折优惠，详询官网客服400-9020-507或联系相应服务经理！'];

  const ERROR_WECHAT_LOGIN = [1010, '扫码错误，请重新刷新页面并使用微信扫码'];
  const ERROR_WECHAT_NOT_EXIST = [1011, '未绑定微信，请使用账号登入'];

  /**
   * error_code 1201-1300 审批相关错误
   */

  const UNKNOW_APM = [1201, '流程未选择,请查看是否未选择业务部门'];
  const ERROR_AUDIT_LEVEL = [1202, '此流程您已审批，已进入下一阶段，请勿重复提交'];
  const UNKNOW_APM_STEP = [1203, '请先设置审批步骤'];
  const UNKNOW_DEPARTMENT_MANAGER = [1204, '部门经理不存在，请联系系统管理员，前往组织管理->部门管理，设置部门的部门经理'];
  const UNKNOW_DUTYID = [1205, '审批流程未设置审批岗位信息'];
  const UNKNOW_AUDIT_USER = [1206, '未选择审核人'];
  const UNKNOW_AUDIT_NEXT_USER = [1207, '下一步骤审核人/处理人不存在，请联系企业管理员进行流程步骤设置'];
  const UNKNOW_AUDIT_DATA = [1208, '业务数据不存在，请截图联系维护人员处理'];
  const UNKNOW_POSITION_MANAGER = [1209, ''];//审批岗位没有对应的人员
  const UNKNOW_PROJECT_MANAGER = [1210, '项目经理不存在，请设置业务关联项目的项目经理，或者更改流程的步骤审批模式'];
  const UNKNOW_LEADER = [1211, '分管领导不存在，请联系系统管理员，前往组织管理->部门管理，设置业务申请人所处部门的分管领导，或者更改流程的步骤审批模式'];
  const UNKNOW_ACTION_NAME = [1212, '操作类型不能为空'];
  const AUDIT_HAS_COMPLETE = [1213, '此流程您已审批完成，请勿重复操作'];
  const AUDIT_CAN_NOT_RECALL = [1214, '此流程正在审批中，不可撤回'];
  const AUDIT_MULTI = [1215, ''];//快速审批
  const AUDIT_MULTI_DOING = [1216, '快速审批仅适用固定流程且审批中的业务'];//快速审批仅适用审批中的业务
  const UNKNOW_DEPARTMENT_HEAD = [1217, '本部主管不存在，请设置业务关联项目的本部主管，或者更改流程的步骤审批模式'];


  /**
   *
   */
  const DATA_PROCESSED = [5000, '您的操作过于频繁，请稍等几秒后再进行操作'];//您的操作过于频繁，请稍等几秒后再进行操作

}
