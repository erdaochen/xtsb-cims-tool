<?php
/**
 * API抛出错误处理
 */

namespace Xtsb\Cims\Exception;

class ApiException extends \RuntimeException
{

  public function __construct(array $apiErrorConst, string $msg = null, \Throwable $previous = null)
  {
    if (!empty($apiErrorConst)) {
      $code = $apiErrorConst[0];
      $message = isset($msg) ? $msg . ($code === 100 ? '：' : null) . $apiErrorConst[1] : $apiErrorConst[1];
    } else {
      $code = 1;
      $message = $msg;
    }

    parent::__construct($message, $code, $previous);
  }
}
