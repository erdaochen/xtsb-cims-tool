<?php
/**
 * Grade.php
 * Notes:分支机构等级
 * author: chen
 * DateTime: 2022/6/15 9:23
 * @package Xtsb\Cims\Branch
 */

namespace Xtsb\Cims\Branch;

use think\facade\Db;

class Grade
{

//  const LIST = [
//    ['value' => -1, 'label' => '通用'],
//    ['value' => 0, 'label' => '总部'],
//    ['value' => 1, 'label' => '一级机构'],
//    ['value' => 2, 'label' => '二级机构'],
//    //  ['value' => 3, 'label' => '三级机构'],
//  ];

  /**
   * 企业分支机构等级
   * @return array[]
   */
  private static function branchGrades()
  {
    $maxGrade = Db::name('branch')->where('cuid', CUID)
      ->where('grade', '>', 0)->max('grade');
    $branchGrades = [
      ['value' => -1, 'label' => '通用'],
      ['value' => 0, 'label' => '总部'],
    ];
    for ($i = 1; $i <= $maxGrade; $i++) {
      $branchGrades[] = ['value' => $i, 'label' => $i . ' 级机构'];
    }
    return $branchGrades;
  }

  /**
   * Notes:获取分支机构等级
   * author: chen
   * DateTime: 2022/6/15 9:28
   * @return array[]
   */
  public static function list($isBranch = false)
  {
    $list = self::branchGrades();
    $arr = [];
    if ($isBranch) {
      foreach ($list as $item) {
        if ($item['value'] >= IS_BRANCH) {
          //仅显示可操作的范围、等级
          $arr[] = $item;
        }
      }
      return $arr;
    }

    return $list;
  }


  /**
   * Notes:根据value 获取对应label
   * author: chen
   * DateTime: 2022/6/15 9:27
   * @param $value
   * @return mixed|null
   */
  public static function getGradeName($value)
  {
    $list = array_column(self::branchGrades(), 'label', 'value');

    return $list[$value] ?? null;
  }

  public static function gradeNames()
  {
    return array_column(self::branchGrades(), 'value', 'label');
  }


  //获取当前等级+通用等级
  public static function gradeIds($isStr = false)
  {
    $a = [-1, IS_BRANCH];
    if ($isStr) {
      return '(' . implode(',', $a) . ')';
    }
    return $a;
  }

  //流程步骤的岗位范围
  public static function stepGradeList($isBranch = false)
  {
    return array_merge(
      self::list($isBranch),
      [
        ['value' => -2, 'label' => '按业务归属部门匹配（本部门内）'],
        ['value' => -3, 'label' => '按业务归属部门匹配（可向上匹配）'],
      ]
    );
  }
}
