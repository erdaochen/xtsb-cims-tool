<?php
/**
 * Notes:分支机构合作类型
 * author: chen
 * DateTime: 2022/6/15 9:23
 * @package Xtsb\Cims\Branch
 */

namespace Xtsb\Cims\Branch;

class Mode
{

  const LIST = [
    ['value' => 1, 'label' => '我司直属（财务直管）'],
    ['value' => 2, 'label' => '个人合作'],
    ['value' => 3, 'label' => '公司合作'],
    ['value' => 4, 'label' => '我司直属（财务独立核算）'],
  ];

  /**
   * Notes:获取分支机构等级
   * author: chen
   * DateTime: 2022/6/15 9:28
   * @return array[]
   */
  public static function list()
  {
    $list = self::LIST;
    return $list;
  }

  public static function modeIds()
  {
    return array_column(self::LIST, 'label', 'value');
  }

  /**
   * Notes:根据value 获取对应label
   * author: chen
   * DateTime: 2022/6/15 9:27
   * @param $value
   * @return mixed|null
   */
  public static function getModeName($value)
  {
    return self::modeIds()[$value] ?? '--';
  }


  public static function modeNames()
  {
    return array_column(self::LIST, 'value', 'label');
  }

  /**
   * Notes:根据label 获取对应value
   * author: chen
   * DateTime: 2022/6/15 9:27
   * @param $label
   * @return mixed|null
   */
  public static function getModeId($label)
  {
    return self::modeNames()[$label] ?? null;
  }
}
