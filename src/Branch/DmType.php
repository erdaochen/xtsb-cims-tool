<?php
/**
 * DmType.php
 * Notes:
 * author: chen
 * DateTime: 2023/1/14 11:39
 * @package Xtsb\Cims\Branch
 */

namespace Xtsb\Cims\Branch;

class DmType
{
  const LIST = [
    ['value' => 1, 'label' => '职能部门'],
    ['value' => 2, 'label' => '项目部门']
  ];
  /**
   * Notes:获取部门类型
   * author: chen
   * DateTime: 2022/6/15 9:28
   * @return array[]
   */
  public static function list()
  {
    return self::LIST;
  }
}
