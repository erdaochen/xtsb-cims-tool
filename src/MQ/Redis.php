<?php

namespace Xtsb\Cims\MQ;

use think\facade\Cache;

class Redis
{

  public static function audit($uid)
  {
    //缓存下一步审批认
    if (!in_array(request()->ip(), ['127.0.0.1', '::1']) && $uid) {
      Cache::store('redis')->set('audit_' . $uid, 1, 8 * 60 * 60);
    }
  }

  public static function cc($uid)
  {
    //缓存抄送人
    if (!in_array(request()->ip(), ['127.0.0.1', '::1']) && $uid) {
      Cache::store('redis')->set('cc_' . $uid, 1, 8 * 60 * 60);
    }
  }

  public static function news($uid)
  {
    //缓存消息通知人
    if (!in_array(request()->ip(), ['127.0.0.1', '::1']) && $uid) {
      Cache::store('redis')->set('news_' . $uid, 1, 8 * 60 * 60);
    }
  }

  /**
   * 项目任务
   */
  public static function projectTask($uid)
  {
    //缓存消息通知人
    if (!in_array(request()->ip(), ['127.0.0.1', '::1']) && $uid) {
      Cache::store('redis')->set('project_task_' . $uid, 1, 8 * 60 * 60);
    }
  }

}