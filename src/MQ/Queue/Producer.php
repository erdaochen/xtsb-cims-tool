<?php
/**
 * 生产者
 */

namespace Xtsb\Cims\MQ\Queue;

use think\facade\Queue;

class Producer
{
  /**
   * @param $jobData null 格式：['tag'=>'队列名称','data'=>$data]
   * @return void
   */
  public static function generate($jobData = null)
  {

    // 1.当前任务将由哪个类来负责处理。

    //   当轮到该任务时，系统将生成一个该类的实例，并调用其 fire 方法

    $jobHandlerClassName = 'Xtsb\Cims\MQ\Queue\Consumer@fire';


    // 2.当前任务归属的队列名称，如果为新队列，会自动创建

    $jobQueueName = "xtsbJobQueue";


    // 3.当前任务所需的业务数据 . 不能为 resource 类型，其他类型最终将转化为json形式的字符串

    //需要执行的数据格式
    if (is_array($jobData)) {
      $jobData = json_encode($jobData, JSON_UNESCAPED_UNICODE);
    }
//    $jobData = $data ?? rand(100, 9999);

    // 4.将该任务推送到消息队列，等待对应的消费者去执行
    $isPushed = Queue::push($jobHandlerClassName, $jobData, $jobQueueName);

    // $time2wait =  60;

    // $isPushed = Queue::later($time2wait, $jobHandlerClassName , $jobData , $jobQueueName );

    // database 驱动时，返回值为 1|false  ;   redis 驱动时，返回值为 随机字符串|false

    if ($isPushed !== false) {

//      echo date('Y-m-d H:i:s') . " a new Hello Job is Pushed to the MQ" . "<br>";

    } else {

//      echo 'Oops, something went wrong.';

    }

  }
}